// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
animals.sort()
console.log(daftarHewan)

// ["1. Tokek", "2. Komodo", "3. Cicak", "4. Ular", "5. Buaya"]

// Soal 2
/* 
    Tulis kode function di sini
*/
function introduce(data) {
    return "Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.gaming;
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

// Soal 3

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

function hitung_1(str) {
    const count = str.match(/[aeiou]/gi).length;
    return count
}

function hitung_2(str) {
    const count = str.match(/[aeiou]/gi).length;
    return count
}

console.log(hitung_1 , hitung_2) // 3 2
