var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

console.log(pertama.concat(kedua)); // saya sangat senang hari ini belajar javascript itu keren

//////////////////////////////////////////////////////////////////////////////////////////////

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var kataPertama = Number("10");
var kataKedua = Number("2");
var kataKetiga = Number("4");
var kataKeempat = Number("6");

var jumlah = kataPertama + kataKedua * kataKetiga + kataKeempat;
console.log(jumlah); // 24

//////////////////////////////////////////////////////////////////////////////////////////////

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); ; // do your own! 
var kataKetiga = kalimat.substring(14, 18); ; // do your own! 
var kataKeempat = kalimat.substring(18, 24); ; // do your own! 
var kataKelima = kalimat.substring(31, 24); ; // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// "Kata Pertama: wah"
// "Kata Kedua: javascript"
// "Kata Ketiga:  itu"
// "Kata Keempat:  keren"
// "Kata Kelima:  sekali"
