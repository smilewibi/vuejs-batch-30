// Soal 1
const hitung_luas = (p, l) => {
 let luas = 1
 let keliling = 1
 luas = p*l
 keliling = 2 * (p + l)   
 console.log('luas:', luas)
 console.log('keliling:', keliling)
}
const panjang = 3;
const lebar = 8;
hitung_luas(panjang, lebar)

// Soal 2
const newFunction = (firstName, lastName) =>{
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 

// Soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};
const {firstName, lastName, address, hobby} = newObject
// Driver code
console.log(firstName, lastName, address, hobby)

// Soal 4
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west,...east]
//Driver Code
console.log(combined)

// Soal 5
const planet = "earth" 
const view = "glass" 
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
//Driver Code
console.log(before)

