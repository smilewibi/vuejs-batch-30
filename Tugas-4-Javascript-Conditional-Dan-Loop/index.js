// Soal 1

var nilai;
var index;

nilai = 70;

if( nilai >=85){
    index = "A";
} else if(nilai >= 75 && nilai < 85) {
    index = "B";
} else if(nilai >= 65 && nilai < 75) {
    index = "C";
} else if(nilai >= 55 && nilai < 65) {
    index = "D";
} else {
    index = "E"
}

console.log("indexnya " + index); // indexnya C

// Soal 2
var tanggal = 22;
var bulan = 7;
var tahun = 2020;

var bulan_string

switch (bulan) {
    case 1:
        bulan_string = "January"
    break;
    case 2:
        bulan_string = "February"
    break;
    case 3:
        bulan_string = "March"
    break;
    case 4:
        bulan_string = "April"
    break;
    case 5:
        bulan_string = "May"
    break;
    case 6:
        bulan_string = "June"
    break;
    case 7:
        bulan_string = "July"
    break;
    case 8:
        bulan_string = "August"
    break;
    case 9:
        bulan_string = "September"
    break;
    case 10:
        bulan_string = "October"
    break;
    case 11:
        bulan_string = "November"
    break;
    default:
        bulan_string = "December"
    break;
}

console.log(tanggal + " " + bulan_string + " " + tahun); // 22 July 2020

// Soal 3
var n = 7

for (var i = 1; i < n+1; i++) {
    var pagar = ''

    for (var j = 0; j < i; j++){
        pagar = pagar + '#'
    }
    console.log(pagar);
}

// "#"
// "##"
// "###"
// "####"
// "#####"
// "######"
// "#######"

// Soal 4
var m = 7

for (var i = 1; index <= m; i++){
    j = i % 3

    switch (j) {
        case 1:
            console.log(i + " - I Love Programming")
        break;
        case 2:
            console.log(i + " - I Love Javascript")
        break;
        default:
            console.log(i + " - I Love VueJS")
            var sama_dengan = ""

            for (var k = 0; k < i; k++) {
                sama_dengan = sama_dengan + '='
            }

            console.log(sama_dengan)
        break;
    }
}

// "1 - I Love Programming"
// "2 - I Love Javascript"
// "3 - I Love VueJS"
// "==="
// "4 - I Love Programming"
// "5 - I Love Javascript"
// "6 - I Love VueJS"
// "======"
// "7 - I Love Programming"